from scipy.optimize import newton

from scipy_steadystate.numerics import numerical_integration_iterator


def backward_euler(df, x0, dt, t0=0.0, dfprime=None, dfprime2=None, x0s=None):
    def y(t, x):
        fprime = None if dfprime is None else lambda y: dt * dfprime(t+dt, y) - 1
        fprime2 = None if dfprime2 is None else lambda y: dt * dfprime2(t+dt, y)

        return newton(lambda xn: x - xn + dt * df(t+dt, xn),
                      x0=x, x1=x0s, fprime=fprime, fprime2=fprime2)

    return numerical_integration_iterator(y, x0, dt, t0)
