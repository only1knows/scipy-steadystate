from scipy_steadystate.numerics import numerical_integration_iterator


def forward_euler(df, x0, dt, t0=0.0):
    def y(t, x):
        return x + dt * df(t, x)

    return numerical_integration_iterator(y, x0, dt, t0)
