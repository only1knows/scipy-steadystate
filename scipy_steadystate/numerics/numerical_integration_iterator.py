from itertools import count


def numerical_integration_iterator(f, x0, dt, t0=0.0):
    x = x0
    for i in count():
        x = f(t0 + dt * i, x)
        yield x
