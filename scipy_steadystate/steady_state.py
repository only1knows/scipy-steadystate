from scipy.optimize import newton

from scipy_steadystate.itertools import take, pairwise
from scipy_steadystate.numerics import prod
from scipy_steadystate.numerics.algorithms import backward_euler, forward_euler, midpoint_method, trapezoidal_rule


def steady_state(sf,
                 T,
                 x0,
                 t0=0.0,
                 steps=10,
                 sfprime=None,
                 sfprime2=None,
                 x0s=None,
                 integration_method='forward-euler'):
    """
    Finds the periodic steady state solution of a given state equation system.

    A periodic steady state is found when the state a t=t0 is equal to the one after the
    periodic time t+T, i.e. sf(t0) = sf(t0 + T). This method explicitly is for non LTI-systems,
    however the system must be assembled out of periodic systems

    Periodic steady states usually happen only if all elements comprising the system are
    themselves periodic. If more than one periodic state source exists, the overall periodic time
    T will become the least common multiple (LCM) extended for decimals.
    E.g.
    - for one periodic source having an intrinsic time of T1 = 2s, T = T1 = 2s.
    - for two periodic sources: T1 = 2s, T2 = 3s, T = LCM(T1, T2) = 6s
    - and so on.

    :param sf:
        The state equations. Must be a function with two input parameters; time `t` and state `x`.
    :param T:
        The intrinsic period time T known ahead of computing steady state.
    :param t0:
        The start time t0. For proper systems setting this value should be unnecessary.
    :param x0:
        Initial guess for the steady state solution.
    :param steps:
        The number of computation steps to perform for numerical integration of the steady equations.
        The more steps performed, the more accurate the result with less artifacts but the slower the computation.
    :param sfprime:
        Separate function providing the first derivative of `sf`. Takes two arguments `t` and `x`.
    :param sfprime2:
        Separate function providing the second derivative of `sf`. Takes two arguments `t` and `x`.
    :param integration_method:
        The integration method to perform. Supported values are:
        - `euler`/`forward-euler`: Performs the most simple form of integration: x2 = x1 + dt * sf(t, x1)
        - `backward-euler`: Simplest implicit integration method: x2 = x1 + dt * sf(t+dt, x2)
        - `trapezoidal`/`trapezoidal-rule`: Trapezoidal rule which averages the value between the current and next step:
          x2 = x1 + dt / 2 * (sf(t, x1) + sf(t+dt, x2))
        - `midpoint`/`midpoint-method`: Midpoint method that uses the value between the current and next step:
          x2 = x1 + dt * sf(t+dt/2, (x1+x2)/2)
    :return:
    """
    dt = T / steps

    if integration_method == 'forward-euler' or integration_method == 'euler':
        def create_integration_series(x0):
            return forward_euler(sf, x0, dt, t0)

        # TODO Find a better abstraction for calculating derivatives of numerical integration methods.
        def integration_method_derivative(t, x, y):
            return 1 + dt * sfprime(t, x)

        def integration_method_derivative2(t, x, y):
            return dt * sfprime2(t, x)

    elif integration_method == 'backward-euler':
        def create_integration_series(x0):
            return backward_euler(sf, x0, dt, t0, sfprime, sfprime2, x0s)

        def integration_method_derivative(t, x, y):
            return 1 / (1 - dt * sfprime(t+dt, y))

        def integration_method_derivative2(t, x, y):
            return dt * sfprime2(t+dt, y) * integration_method_derivative(t, x, y)**2 / (1 - dt * sfprime(t+dt, y))

    elif integration_method == 'trapezoidal' or integration_method == 'trapezoidal-rule':
        def create_integration_series(x0):
            return trapezoidal_rule(sf, x0, dt, t0, sfprime, sfprime2, x0s)

        def integration_method_derivative(t, x, y):
            return (1 + 0.5 * dt * sfprime(t, x)) / (1 - 0.5 * dt * sfprime(t+dt, y))

        def integration_method_derivative2(t, x, y):
            return 0.5 * dt * (sfprime2(t, x) + integration_method_derivative(t, x, y)**2 * sfprime2(t+dt, y)) / (1 - 0.5 * dt * sfprime(t+dt, y))

    elif integration_method == 'midpoint' or integration_method == 'midpoint-method':
        def create_integration_series(x0):
            return midpoint_method(sf, x0, dt, t0, sfprime, sfprime2, x0s)

        def integration_method_derivative(t, x, y):
            p = 0.5 * (x + y)
            tn = t + dt * 0.5
            return (1 + dt * sfprime(tn, p)) / (1 - dt * sfprime(tn, p))

        def integration_method_derivative2(x, y):
            p = 0.5 * (x + y)
            tn = t + dt * 0.5
            return 0.25 * dt * sfprime2(tn, p) * (1 + integration_method_derivative(t, x, y)**2) / (1 - 0.5 * dt * sfprime(tn, p))

    else:
        raise ValueError(f'unknown integration method: {integration_method}')

    # FIXME As long as scipy doesn't support returning derivatives from inside the problem function, we have to
    #  inefficiently recompute the series inside the derivatives. See https://github.com/scipy/scipy/issues/11893
    #  Note that this code below didn't get updated completely during the development process and needs adjustments
    #  the time it can be enabled again.
    #def f(x0):
    #    method = create_integration_series(x0)
    #    vals = [x0] + take(steps, method)
    #
    #    returnval = vals[-1]
    #    if sfprime is not None:
    #        d = prod(integration_method_derivative(x1, x2) for x1, x2 in pairwise(vals))
    #        returnval = (returnval, d)
    #    if sfprime2 is not None:
    #        dd = prod(integration_method_derivative2(x1, x2) for x1, x2 in pairwise(vals))
    #        returnval = returnval + (dd,)
    #
    #    return returnval
    def f(x0):
        method = create_integration_series(x0)
        vals = take(steps, method)  # TODO Replace with discarding iterator.
        return vals[-1] - x0
    def df(x0):
        method = create_integration_series(x0)
        vals = [x0] + take(steps, method)
        d = prod(integration_method_derivative(t0+dt*i, x1, x2) for i, (x1, x2) in enumerate(pairwise(vals))) - 1
        return d
    def ddf(x0):
        method = create_integration_series(x0)
        vals = [x0] + take(steps, method)
        dd = prod(integration_method_derivative2(t0+dt*i, x1, x2) for i, (x1, x2) in enumerate(pairwise(vals)))
        return dd

    result = newton(f,
                    x0,
                    fprime=None if sfprime is None else df,
                    fprime2=None if sfprime2 is None else ddf,
                    x1=x0s)

    return result
